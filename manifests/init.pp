class mpwarmoduleexam {
  #APACHE
  class{ 'apache': }

  apache::vhost { 'myMpwar.dev':
    port          => '80',
    docroot       => '/web',
  }

  #PHP
  include ::yum::repo::remi_php55

  class { 'php':
    version => 'latest',
    require => Yumrepo['remi-php55']
  }

  #Instalar MySQL y crear base de datos llamada mympwar
  class { 'mysql::server':
    root_password => 'root',
    databases     => {
      'mympwar' =>{
        ensure => 'present',
        charset => 'utf8',
      }
    },
  }

  #Instalación memcached
  include memcached

  #Añadir ficheros php
  file { '/web/index.php':
    ensure  => file,
    replace => true,
    content => "<?php echo 'Hello World. Sistema operativo ${operatingsystem} ${operatingsystemrelease}';",
    mode => 777,
  }
  file { "/web/info.php":
    ensure  => file,
    replace => true,
    content => "<?php phpinfo();",
    mode => 777,
  }
  file { "/web":
    ensure => directory,
    mode => 777,
  }
}